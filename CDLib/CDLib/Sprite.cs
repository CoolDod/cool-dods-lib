﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace CDLib
{
    public class Sprite
    {
        // [DATA]
        Texture2D texture;
        Rectangle destinationRectangle;
        Vector2 position;
        Color colour;

        Vector2 origin = Vector2.Zero;
        Vector2 scale = Vector2.One;
        Rectangle? sourceRectangle = null;
        SpriteEffects effects = SpriteEffects.None;
        float rotation = 0f;
        float depth = 0f;

        // [METHODS]

        // CONSTRUCTORS
        public Sprite(Texture2D _texture, Rectangle _destinationRectangle, Color _colour)
        {
            texture = _texture;
            destinationRectangle = _destinationRectangle;
            colour = _colour;

            position = new Vector2(destinationRectangle.X, destinationRectangle.Y);
            scale = new Vector2(destinationRectangle.Width / texture.Width, destinationRectangle.Height / texture.Height);
        }

        public Sprite(Texture2D _texture, Vector2 _position, Color _colour)
        {
            texture = _texture;
            position = _position;
            colour = _colour;

            destinationRectangle = new Rectangle(position.ToPoint(), new Point(texture.Width, texture.Height));
        }

        public Sprite(Texture2D _texture, Rectangle _destinationRectangle, Rectangle? _sourceRectangle, Color _colour)
        {
            texture = _texture;
            destinationRectangle = _destinationRectangle;
            sourceRectangle = _sourceRectangle;
            colour = _colour;

            position = new Vector2(destinationRectangle.X, destinationRectangle.Y);
            scale = new Vector2(destinationRectangle.Width / texture.Width, destinationRectangle.Height / texture.Height);
        }

        public Sprite(Texture2D _texture, Vector2 _position, Rectangle? _sourceRectangle, Color _colour)
        {
            texture = _texture;
            position = _position;
            colour = _colour;

            destinationRectangle = new Rectangle(position.ToPoint(), new Point(texture.Width, texture.Height));
            sourceRectangle = _sourceRectangle;
        }

        public Sprite(Texture2D _texture, Rectangle _destinationRectangle, Rectangle? _sourceRectangle, Color _colour, float _rotation, Vector2 _origin, SpriteEffects _effects, float _layerDepth)
        {
            texture = _texture;
            destinationRectangle = _destinationRectangle;
            sourceRectangle = _sourceRectangle;
            colour = _colour;
            rotation = _rotation;
            origin = _origin;
            effects = _effects;
            depth = _layerDepth;

            position = new Vector2(destinationRectangle.X, destinationRectangle.Y);
            scale = new Vector2(destinationRectangle.Width / texture.Width, destinationRectangle.Height / texture.Height);
        }

        public Sprite(Texture2D _texture, Vector2 _position, Rectangle? _sourceRectangle, Color _colour, float _rotation, Vector2 _origin, float _scale, SpriteEffects _effects, float _layerDepth)
        {
            texture = _texture;
            position = _position;
            sourceRectangle = _sourceRectangle;
            colour = _colour;
            rotation = _rotation;
            origin = _origin;
            scale = new Vector2(_scale);
            effects = _effects;
            depth = _layerDepth;

            destinationRectangle = new Rectangle(position.ToPoint(), new Point(texture.Width, texture.Height));
        }

        public Sprite(Texture2D _texture, Vector2 _position, Rectangle? _sourceRectangle, Color _colour, float _rotation, Vector2 _origin, Vector2 _scale, SpriteEffects _effects, float _layerDepth)
        {
            texture = _texture;
            position = _position;
            sourceRectangle = _sourceRectangle;
            colour = _colour;
            rotation = _rotation;
            origin = _origin;
            scale = _scale;
            effects = _effects;
            depth = _layerDepth;

            destinationRectangle = new Rectangle(position.ToPoint(), new Point(texture.Width, texture.Height));
        }


        // Draw
        public void Draw(SpriteBatch batch, bool useRect)
        {
            if (useRect)
                batch.Draw(texture, destinationRectangle, sourceRectangle, colour, rotation, origin, effects, depth);
            else
                batch.Draw(texture, position, sourceRectangle, colour, rotation, origin, scale, effects, depth);
        }
    }
}
